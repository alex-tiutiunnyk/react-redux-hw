import {combineReducers} from 'redux';
import messages from '../messages/reducer';
import messagePage from '../messagePage/reducer';

const rootReducer = combineReducers({
    messages,
    messagePage,

});

export default rootReducer;
