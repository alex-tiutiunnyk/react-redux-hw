const getNewId = () => (new Date()).getTime()*1000;

export default {
    getNewId
};
