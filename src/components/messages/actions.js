import {ADD_MESSAGE, DELETE_MESSAGE, LIKE_MESSAGE, UPDATE_MESSAGE} from "./actionTypes";
import service from './service';

export const addMessage = data => ({
    type: ADD_MESSAGE,
    payload: {
        id: service.getNewId(),
        data

    }
});

export const updateMessage = (id, data) => ({
    type: UPDATE_MESSAGE,
    payload: {
        id,
        data
    }
});

export const deleteMessage = id => ({
    type: DELETE_MESSAGE,
    payload: {
        id
    }
});

export const likeMessage = id => ({
    type: LIKE_MESSAGE,
    payload: {
        id
    }
});