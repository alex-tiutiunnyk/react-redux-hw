import React from "react";
import Header from "./messages/Header";
import MessageInput from "./messages/MessageInput";
import MessageList from "./messages/MessageList";
import "../App.css"

class Chat extends React.Component {

    render() {
        /*const [messages, setMessages] = React.useState([]);
        const [loading, setLoading] = React.useState(true);

        useEffect(() => {
            fetch(props.url)
                .then((response) => response.json())
                .then((messages) => {
                    setTimeout(() => {
                        setMessages(messages);
                        setLoading(false);
                    }, 2000);
                });
        }, []);*/

        return (
            <div>
                <Header/>
                <MessageList/>
                <MessageInput/>
            </div>
        )
    }
}

export default Chat;
